/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author pdt004
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t) 
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return super.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		float x = super.getValue();
		Temperature temp = new Celsius(x);
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		float x = super.getValue();
		x = (float) ((x*(float)9/5)+32);
		Temperature temp = new Fahrenheit(x);
		return temp;
	}
	public Temperature toKelvin() {
		return null;
	}
}
