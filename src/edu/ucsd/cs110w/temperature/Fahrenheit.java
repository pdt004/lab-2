/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author pdt004
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) 
	{
		super(t);
	}
	public String toString()
	{
		return super.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		float x = super.getValue();
		x = (float) ((x-32)*((float)5/9));
		Temperature temp = new Celsius(x);
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		float x = super.getValue();
		Temperature temp = new Fahrenheit(x);
		return temp;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}

