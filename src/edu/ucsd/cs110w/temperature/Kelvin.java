/**
 * 
 */
package edu.ucsd.cs110w.temperature;


/**
 * TODO (pdt004): write class javadoc
 *
 * @author pdt004
 */
public class Kelvin extends Temperature{
	
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return super.getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		// TODO: Complete this method
		float x = super.getValue();
		float kelvinConv = 273.15f;
		x = x - kelvinConv;
		Temperature temp = new Celsius(x);
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		float x = super.getValue();
		float kelvinConv = 459.67f;
		x = (x * 1.8f) - kelvinConv;
		Temperature temp = new Fahrenheit(x);
		return temp;
	}
	
	public Temperature toKelvin() {
		return null;
	}
}
